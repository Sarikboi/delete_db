-- Remove a previously inserted film from the inventory and all corresponding rental records
DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = (SELECT film_id FROM film WHERE title = 'The Fast and The Furious')
);

DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'The Fast and The Furious');

-- Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"
DELETE FROM payment
WHERE customer_id IN (
    SELECT customer_id
    FROM customer
    WHERE first_name = 'Sardorbek' AND last_name = 'Tukhtasinov'
);

DELETE FROM rental
WHERE customer_id IN (
    SELECT customer_id
    FROM customer
    WHERE first_name = 'Sardorbek' AND last_name = 'Tukhtasinov'
);








